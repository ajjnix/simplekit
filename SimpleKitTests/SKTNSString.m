//
// Copyright (c) 2016 Artem Mylnikov (ajjnix)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <XCTest/XCTest.h>
#import "NSString+SimpleKit.h"
#import <objc/runtime.h>


@interface SKTNSString : XCTestCase

@property (assign, nonatomic) NSInteger propertyNSInteger;

@end


@implementation SKTNSString

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_stringWithFormat_array {
    NSString *expected = @"Hello world";
    NSString *result = [NSString skt_stringWithFormat:@"H%@o %@" array:@[@"ell", @"world"]];
    XCTAssertEqualObjects(result, expected);
}

- (void)test_propertyToString {
    u_int count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    XCTAssertEqual(count, 1);
    
    NSString *propertyName = [NSString skt_propertyToString:&properties[0]];
    free(properties);
    
    XCTAssertEqualObjects(propertyName, @"propertyNSInteger");
}

- (void)test_getterToSetter {
    NSString *expect = @"setGetter:";
    NSString *result = [@"getter" skt_getterToSetter];
    XCTAssertEqualObjects(result, expect);
}

@end

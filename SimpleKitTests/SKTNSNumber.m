//
// Copyright (c) 2016 Artem Mylnikov (ajjnix)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <XCTest/XCTest.h>
#import "NSNumber+SimpleKit.h"


#ifndef SKTAccuracy
    #define SKTAccuracy (1.0e-7)
#endif


@interface SKTNSNumber : XCTestCase
@end


@implementation SKTNSNumber

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


#pragma mark - sum

- (void)testSumInteger {
    NSNumber *expect = @4;
    NSNumber *result = [@1 skt_sumInteger:3];
    XCTAssertEqualObjects(result, expect);
}

- (void)testSumIntegerNumber {
    NSNumber *expect = @4;
    NSNumber *result = [@1 skt_sumIntegerNumber:@3];
    XCTAssertEqualObjects(result, expect);
}

- (void)testSumFloat {
    NSNumber *expect = @4.2;
    NSNumber *result = [@1 skt_sumFloat:3.2];
    XCTAssertEqualWithAccuracy([expect floatValue], [result floatValue], SKTAccuracy);
}

- (void)testSumFloatNumber {
    NSNumber *expect = @4.2;
    NSNumber *result = [@1 skt_sumFloatNumber:@3.2];
    XCTAssertEqualWithAccuracy([expect floatValue], [result floatValue], SKTAccuracy);
}


#pragma mark - sub

- (void)testSubInteger {
    NSNumber *expect = @1;
    NSNumber *result = [@4 skt_subInteger:3];
    XCTAssertEqualObjects(result, expect);
}

- (void)testSubIntegerNumber {
    NSNumber *expect = @1;
    NSNumber *result = [@4 skt_subIntegerNumber:@3];
    XCTAssertEqualObjects(result, expect);
}

- (void)testSubFloat {
    NSNumber *expect = @1;
    NSNumber *result = [@4.2 skt_subFloat:3.2];
    XCTAssertEqualWithAccuracy([expect floatValue], [result floatValue], SKTAccuracy);
}

- (void)testSubFloatNumber {
    NSNumber *expect = @1;
    NSNumber *result = [@4.2 skt_subFloatNumber:@3.2];
    XCTAssertEqualWithAccuracy([expect floatValue], [result floatValue], SKTAccuracy);
}


#pragma mark - div

- (void)testDivInteger {
    NSNumber *expect = @1;
    NSNumber *result = [@4 skt_divInteger:4];
    XCTAssertEqualObjects(result, expect);
    XCTAssertEqualObjects([@5 skt_divInteger:2], @2);
}

- (void)testDivIntegerNumber {
    NSNumber *expect = @1;
    NSNumber *result = [@4 skt_divIntegerNumber:@4];
    XCTAssertEqualObjects(result, expect);
    XCTAssertEqualObjects([@5 skt_divIntegerNumber:@2], @2);
}

- (void)testDivFloat {
    NSNumber *expect = @1;
    NSNumber *result = [@4.2 skt_divFloat:4.2];
    XCTAssertEqualWithAccuracy([expect floatValue], [result floatValue], SKTAccuracy);
    XCTAssertEqualWithAccuracy([[@5 skt_divFloat:2] floatValue], [@2.5 floatValue], SKTAccuracy);
}

- (void)testDivFloatNumber {
    NSNumber *expect = @1;
    NSNumber *result = [@4.2 skt_divFloatNumber:@4.2];
    XCTAssertEqualWithAccuracy([expect floatValue], [result floatValue], SKTAccuracy);
    XCTAssertEqualWithAccuracy([[@5 skt_divFloatNumber:@2] floatValue], [@2.5 floatValue], SKTAccuracy);
}


#pragma mark - mul

- (void)testMulInteger {
    NSNumber *expect = @4;
    NSNumber *result = [@2 skt_mulInteger:2];
    XCTAssertEqualObjects(result, expect);
}

- (void)testMulIntegerNumber {
    NSNumber *expect = @4;
    NSNumber *result = [@2 skt_mulIntegerNumber:@2];
    XCTAssertEqualObjects(result, expect);
}

- (void)testMulFloat {
    NSNumber *expect = @5;
    NSNumber *result = [@2 skt_mulFloat:2.5];
    XCTAssertEqualWithAccuracy([expect floatValue], [result floatValue], SKTAccuracy);
    XCTAssertEqualWithAccuracy([[@2.5 skt_mulFloat:3] floatValue], [@7.5 floatValue], SKTAccuracy);
}

- (void)testMulFloatNumber {
    NSNumber *expect = @5;
    NSNumber *result = [@2 skt_mulFloatNumber:@2.5];
    XCTAssertEqualWithAccuracy([expect floatValue], [result floatValue], SKTAccuracy);
    XCTAssertEqualWithAccuracy([[@2.5 skt_mulFloatNumber:@3] floatValue], [@7.5 floatValue], SKTAccuracy);
}

@end

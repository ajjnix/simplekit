//
// Copyright (c) 2016 Artem Mylnikov (ajjnix)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "NSNumber+SimpleKit.h"


@implementation NSNumber (SimpleKit)

#pragma mark - sum

- (NSNumber *)skt_sumInteger:(NSInteger)value {
    return @([self integerValue] + value);
}

- (NSNumber *)skt_sumIntegerNumber:(NSNumber *)value {
    return @([self integerValue] + [value integerValue]);
}

- (NSNumber *)skt_sumFloat:(CGFloat)value {
    return @([self doubleValue] + value);
}

- (NSNumber *)skt_sumFloatNumber:(NSNumber *)value {
    return @([self doubleValue] + [value doubleValue]);
}


#pragma mark - sub

- (NSNumber *)skt_subInteger:(NSInteger)value {
    return @([self integerValue] - value);
}

- (NSNumber *)skt_subIntegerNumber:(NSNumber *)value {
    return @([self integerValue] - [value integerValue]);
}

- (NSNumber *)skt_subFloat:(CGFloat)value {
    return @([self doubleValue] - value);
}

- (NSNumber *)skt_subFloatNumber:(NSNumber *)value {
    return @([self doubleValue] - [value doubleValue]);
}


#pragma mark - div

- (NSNumber *)skt_divInteger:(NSInteger)value {
    return @([self integerValue] / value);
}

- (NSNumber *)skt_divIntegerNumber:(NSNumber *)value {
    return @([self integerValue] / [value integerValue]);
}

- (NSNumber *)skt_divFloat:(CGFloat)value {
    return @([self doubleValue] / value);
}

- (NSNumber *)skt_divFloatNumber:(NSNumber *)value {
    return @([self doubleValue] / [value doubleValue]);
}


#pragma mark - mul

- (NSNumber *)skt_mulInteger:(NSInteger)value {
    return @([self integerValue] * value);
}

- (NSNumber *)skt_mulIntegerNumber:(NSNumber *)value {
    return @([self integerValue] * [value integerValue]);
}

- (NSNumber *)skt_mulFloat:(CGFloat)value {
    return @([self doubleValue] * value);
}

- (NSNumber *)skt_mulFloatNumber:(NSNumber *)value {
    return @([self doubleValue] * [value doubleValue]);
}

@end

//
// Copyright (c) 2016 Artem Mylnikov (ajjnix)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGGeometry.h>


extern void stk_functionPrintHelloWorld();
typedef void(*STKSimpleFunction)();


@interface SKTAutoMappingDataSimpleA : NSObject

@property (strong, nonatomic) NSNumber *numberFloat;
@property (strong, nonatomic) NSNumber *numberInt;

@property (assign, nonatomic) char charValue;
@property (assign, nonatomic) int intValue;
@property (assign, nonatomic) short shortValue;
@property (assign, nonatomic) long longValue;
@property (assign, nonatomic) long long longlongValue;
@property (assign, nonatomic) NSInteger integerValue;

@property (assign, nonatomic, readonly) CGFloat cgfloatValue;
@property (assign, nonatomic, readonly) float floatValue;
@property (assign, nonatomic, readonly) double doubleValue;

@property (assign, nonatomic) BOOL boolValue;
@property (assign, nonatomic) CGPoint point;

@property (assign, nonatomic) void *pvoid;
@property (assign, nonatomic) STKSimpleFunction pfunc;
@property (assign, nonatomic) SEL selector;
@property (assign, nonatomic) Class classNSData;

@end


@interface SKTAutoMappingDataSimpleB : NSObject

@property (strong, nonatomic) NSNumber *numberFloat;
@property (strong, nonatomic) NSNumber *numberInt;

@property (assign, nonatomic) char charValue;
@property (assign, nonatomic) int intValue;
@property (assign, nonatomic) short shortValue;
@property (assign, nonatomic) long longValue;
@property (assign, nonatomic) long long longlongValue;
@property (assign, nonatomic) NSInteger integerValue;

@property (assign, nonatomic) CGFloat cgfloatValue;
@property (assign, nonatomic) float floatValue;
@property (assign, nonatomic) double doubleValue;

@property (assign, nonatomic) BOOL boolValue;
@property (assign, nonatomic) CGPoint point;

@property (assign, nonatomic) void *pvoid;
@property (assign, nonatomic) STKSimpleFunction pfunc;
@property (assign, nonatomic) SEL selector;
@property (assign, nonatomic) Class classNSData;

@end

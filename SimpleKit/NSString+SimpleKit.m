//
// Copyright (c) 2016 Artem Mylnikov (ajjnix)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "NSString+SimpleKit.h"
#import <objc/runtime.h>


@implementation NSString (SimpleKit)

+ (NSString *)skt_stringWithFormat:(NSString *)format array:(NSArray *)array {
    NSMethodSignature *methodSignature = [self skt_generateSignatureForArguments:array];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:methodSignature];
    
    [invocation setTarget:self];
    [invocation setSelector:@selector(stringWithFormat:)];
    
    [invocation setArgument:&format atIndex:2];
    for (NSInteger i = 0; i < [array count]; i++) {
        id obj = array[i];
        [invocation setArgument:(&obj) atIndex:i+3];
    }
    
    [invocation invoke];
    
    __autoreleasing NSString *string;
    [invocation getReturnValue:&string];
    
    return string;
}

//https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtTypeEncodings.html
+ (NSMethodSignature *)skt_generateSignatureForArguments:(NSArray *)arguments {
    NSInteger count = [arguments count];
    NSInteger sizeptr = sizeof(void *);
    NSInteger sumArgInvoke = count + 3; //self + _cmd + (NSString *)format
    NSInteger offsetReturnType = sumArgInvoke * sizeptr;
    
    NSMutableString *mstring = [[NSMutableString alloc] init];
    [mstring appendFormat:@"@%zd@0:%zd", offsetReturnType, sizeptr];
    for (NSInteger i = 2; i < sumArgInvoke; i++) {
        [mstring appendFormat:@"@%zd", sizeptr * i];
    }
    return [NSMethodSignature signatureWithObjCTypes:[mstring UTF8String]];
}

+ (NSString *)skt_propertyToString:(void *)ptr {
    objc_property_t *propertyPtr = (objc_property_t *)ptr;
    const char *cname = property_getName(*propertyPtr);
    return [NSString stringWithCString:cname encoding:NSUTF8StringEncoding];
}

- (NSString *)skt_getterToSetter {
    NSString *capitalLetter = [[self substringToIndex:1] uppercaseString];
    return [NSString stringWithFormat:@"set%@%@:", capitalLetter, [self substringFromIndex:1]];
}

@end

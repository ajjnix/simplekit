//
// Copyright (c) 2016 Artem Mylnikov (ajjnix)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "NSSet+SimpleKit.h"
#import "NSString+SimpleKit.h"
#import <objc/runtime.h>


@implementation NSSet (SimpleKit)

- (NSSet *)skt_setByIntersectSet:(NSSet *)set {
    NSMutableSet *mset = [self mutableCopy];
    [mset intersectSet:set];
    return mset;
}

+ (NSSet *)skt_allPropertyNameBeforeRootClass:(id)object {
    NSMutableSet *mset = [NSMutableSet set];
    
    Class class = [object class];
    while (class != Nil && class_getSuperclass(class) != nil) {
        u_int count;
        objc_property_t *properties = class_copyPropertyList(class, &count);
        for (u_int i = 0; i < count ; i++) {
            objc_property_t property = properties[i];
            NSString *propertyName = [NSString skt_propertyToString:&property];
            [mset addObject:propertyName];
        }
        free(properties);
        class = class_getSuperclass(class);
    }
    
    return mset;
}

@end

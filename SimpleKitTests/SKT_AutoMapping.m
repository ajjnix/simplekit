//
// Copyright (c) 2016 Artem Mylnikov (ajjnix)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <XCTest/XCTest.h>
#import "AutoMapping+SimpleKit.h"
#import "SKTAutoMappingDataSimple.h"
#import "SKTAutoMappingDataSubclass.h"
#import "SKTAutoMappingDataPartial.h"


#ifndef SKTAccuracy
#define SKTAccuracy (1.0e-7)
#endif


@interface SKT_AutoMapping : XCTestCase
@end


@implementation SKT_AutoMapping

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_autoMappingSimple {
    SKTAutoMappingDataSimpleA *source = [[SKTAutoMappingDataSimpleA alloc] init];
    SKTAutoMappingDataSimpleB *target = [[SKTAutoMappingDataSimpleB alloc] init];
    
    id result = skt_autoMapping(source, target);
    XCTAssertEqual(result, target);
    
    XCTAssertEqualObjects([source numberFloat], [target numberFloat]);
    XCTAssertEqualObjects([source numberInt], [target numberInt]);
    
    XCTAssertEqual([source charValue], [target charValue]);
    XCTAssertEqual([source intValue], [target intValue]);
    XCTAssertEqual([source shortValue], [target shortValue]);
    XCTAssertEqual([source longValue], [target longValue]);
    XCTAssertEqual([source longlongValue], [target longlongValue]);
    XCTAssertEqual([source integerValue], [target integerValue]);

    XCTAssertEqualWithAccuracy([source cgfloatValue], [target cgfloatValue], SKTAccuracy);
    XCTAssertEqualWithAccuracy([source floatValue], [target floatValue], SKTAccuracy);
    XCTAssertEqualWithAccuracy([source doubleValue], [target doubleValue], SKTAccuracy);

    XCTAssertEqual([source boolValue], [target boolValue]);
    CGPoint p1 = [source point];
    CGPoint p2 = [target point];
    XCTAssertTrue(CGPointEqualToPoint(p1, p2));
    
    XCTAssertEqual([source pvoid], [target pvoid]);
    XCTAssertEqual([source pfunc], [target pfunc]);

    XCTAssertEqual([source pfunc], [target pfunc]);
    XCTAssertEqual([source selector], [target selector]);
    XCTAssertEqual([source classNSData], [target classNSData]);
}

- (void)test_autoMappingWithSubclass {
    SKTAutoMappingDataSubclassA *source = [[SKTAutoMappingDataSubclassA alloc] init];
    SKTAutoMappingDataSubclassB *target = [[SKTAutoMappingDataSubclassB alloc] init];
    
    skt_autoMapping(source, target);
    
    XCTAssertEqualObjects([source first], [target first]);
    XCTAssertEqualObjects([source second], [target second]);
}

- (void)test_autoMappingPartial {
    SKTAutoMappingDataPartialA *source = [[SKTAutoMappingDataPartialA alloc] init];
    SKTAutoMappingDataPartialB *target = [[SKTAutoMappingDataPartialB alloc] init];
    
    source.first = [[NSMutableArray alloc] init];
    source.second = [[NSString alloc] init];
    source.value = 13;
    
    skt_autoMapping(source, target);
    
    XCTAssertEqualObjects([source first], [target first]);
    XCTAssertNotEqual([source first], [target first]);

    XCTAssertEqualObjects([source second], [target second]);
    XCTAssertEqual([source second], [target second]);
    
    XCTAssertTrue([target value]);
}

- (void)test_returnAutoMappingWithDictionary {
    SKTAutoMappingDataSimpleA *source = [[SKTAutoMappingDataSimpleA alloc] init];
    source.numberInt = @33;
    source.numberFloat = @1.22;
    SKTAutoMappingDataSimpleB *target = [[SKTAutoMappingDataSimpleB alloc] init];
    
    id result = skt_autoMappingWithDictionary(source, target, @{
                                                                @"numberInt": @"numberFloat",
                                                                });
    XCTAssertEqual(result, target);
    XCTAssertEqualObjects(source.numberInt, target.numberFloat);
}

- (void)test_returnAutoMappingWithArray {
    SKTAutoMappingDataSimpleA *source = [[SKTAutoMappingDataSimpleA alloc] init];
    source.longValue = 13;
    source.numberFloat = @33.2;
    source.numberInt = @2;

    SKTAutoMappingDataSimpleB *target = [[SKTAutoMappingDataSimpleB alloc] init];
    target.numberInt = @3;
    
    id result = skt_autoMappingWithArray(source, target, @[@"longValue", @"numberFloat"]);
    XCTAssertEqual(result, target);
    XCTAssertEqualObjects(source.numberFloat, target.numberFloat);
    XCTAssertEqual(source.longValue, target.longValue);
    XCTAssertNotEqualObjects(source.numberInt, target.numberInt);
}

@end

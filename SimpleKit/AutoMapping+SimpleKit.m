// 
// Copyright (c) 2016 Artem Mylnikov (ajjnix)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "AutoMapping+SimpleKit.h"
#import "NSSet+SimpleKit.h"
#import "NSString+SimpleKit.h"


void skt_autoMapping_without_assert(id source, id target);
id skt_autoMapping(id source, id target) {
    NSCParameterAssert(source != nil);
    NSCParameterAssert(target != nil);
    
    skt_autoMapping_without_assert(source, target);
    return target;
}

#define BUFFER_SIZE 1024
void skt_autoMapping_property(id source, id target, NSString *propertyName, void *buffer);
void skt_autoMapping_without_assert(id source, id target) {
    NSSet *allSourcePropertyName = [NSSet skt_allPropertyNameBeforeRootClass:source];
    NSSet *allTargetPropertyName = [NSSet skt_allPropertyNameBeforeRootClass:source];
    
    NSSet *intersect = [allSourcePropertyName skt_setByIntersectSet:allTargetPropertyName];
    void *buffer = malloc(BUFFER_SIZE);
    for (NSString *string in intersect) {
        @try {
            skt_autoMapping_property(source, target, string, buffer);
        } @catch (NSException *exception) {
            NSLog(@"exception %@", [exception reason]);
        }
    }
    free(buffer);
}

void skt_autoMappingWithDictionary_without_assert(id source, id target, NSDictionary *mapping);
id skt_autoMappingWithDictionary(id source, id target, NSDictionary *mapping) {
    NSCParameterAssert(source != nil);
    NSCParameterAssert(target != nil);
    NSCParameterAssert(mapping != nil);
    
    skt_autoMappingWithDictionary_without_assert(source, target, mapping);
    return target;
}

void skt_autoMapping_invoke(id source, id target, SEL getter, SEL setter, void *buffer);
void skt_autoMappingWithDictionary_without_assert(id source, id target, NSDictionary *mapping) {
    void *buffer = malloc(BUFFER_SIZE);
    for (NSString *key in mapping) {
        SEL getterSource = NSSelectorFromString(key);
        NSString *setter = [mapping[key] skt_getterToSetter];
        SEL setterTarget = NSSelectorFromString(setter);
        @try {
            skt_autoMapping_invoke(source, target, getterSource, setterTarget, buffer);
        } @catch (NSException *exception) {
            NSLog(@"exception %@", [exception reason]);
        }
    }
    free(buffer);
}

void skt_autoMappingWithArray_without_assert(id source, id target, NSArray *propertys);
id skt_autoMappingWithArray(id source, id target, NSArray *propertys) {
    NSCParameterAssert(source != nil);
    NSCParameterAssert(target != nil);
    NSCParameterAssert(propertys != nil);
    
    skt_autoMappingWithArray_without_assert(source, target, propertys);
    return target;
}

void skt_autoMappingWithArray_without_assert(id source, id target, NSArray *propertys) {
    void *buffer = malloc(BUFFER_SIZE);
    for (NSString *property in propertys) {
        @try {
            skt_autoMapping_property(source, target, property, buffer);
        } @catch(NSException *exception) {
            NSLog(@"exception %@", [exception reason]);
        }
    }
    free(buffer);
}

void skt_autoMapping_property(id source, id target, NSString *propertyName, void *buffer) {
    NSString *setterString = [propertyName skt_getterToSetter];
    SEL setter = NSSelectorFromString(setterString);
    SEL getter = NSSelectorFromString(propertyName);
    skt_autoMapping_invoke(source, target, getter, setter, buffer);
}

void skt_autoMapping_invoke(id source, id target, SEL getterSource, SEL setterTarget, void *buffer) {
    if (![source respondsToSelector:getterSource] || ![target respondsToSelector:setterTarget]) {
        return;
    }
    
    NSMethodSignature *methodSignatureGetter = [source methodSignatureForSelector:getterSource];
    NSInvocation *invocationGetter = [NSInvocation invocationWithMethodSignature:methodSignatureGetter];
    [invocationGetter setTarget:source];
    [invocationGetter setSelector:getterSource];
    [invocationGetter invoke];
    
    [invocationGetter getReturnValue:buffer];
    
    NSMethodSignature *methodSignatureSetter = [target methodSignatureForSelector:setterTarget];
    NSInvocation *invocationSetter = [NSInvocation invocationWithMethodSignature:methodSignatureSetter];
    [invocationSetter setTarget:target];
    [invocationSetter setSelector:setterTarget];
    [invocationSetter setArgument:buffer atIndex:2];
    [invocationSetter invoke];
}
